provider "aws" {
  region     = "eu-central-1"
}


resource "aws_vpc" "codica-vpc" {
     cidr_block = "10.0.0.0/16"
      tags = {
        Name = "Codica-net"
  }
}

resource "aws_subnet" "front-end-net-eu-central-1a" {
  vpc_id     = aws_vpc.codica-vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "eu-central-1a"
  tags = { 
    Name = "Public-net-eu-central-1a"
  }
}
resource "aws_subnet" "front-end-net-eu-central-1b" {
  vpc_id     = aws_vpc.codica-vpc.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "eu-central-1b"

  tags = {
    Name = "Public-net-eu-central-1b"
  }
}


resource "aws_subnet" "back-end-net-eu-central-1a" {
  vpc_id     = aws_vpc.codica-vpc.id
  cidr_block = "10.0.3.0/24"
  availability_zone = "eu-central-1a"

  tags = {
    Name = "Private-net-back-end-net-eu-central-1a"
  }
}

resource "aws_subnet" "back-end-net-eu-central-1b" {
  vpc_id     = aws_vpc.codica-vpc.id
  cidr_block = "10.0.4.0/24"
  availability_zone = "eu-central-1b"

  tags = {
    Name = "Private-net-back-end-net-eu-central-1b"
  }
}

resource "aws_internet_gateway" "codica-gw" {
  vpc_id = aws_vpc.codica-vpc.id

  tags = {
    Name = "Codica-GW"
  }
}

resource "aws_route_table" "codica-rt" {
  vpc_id = aws_vpc.codica-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.codica-gw.id
  } 

  tags = {
    Name = "RT-Front"
  }
}

resource "aws_route_table_association" "front-end-net-eu-central-1a" {
  subnet_id      = aws_subnet.front-end-net-eu-central-1a.id
  route_table_id = aws_route_table.codica-rt.id
}

resource "aws_route_table_association" "front-end-net-eu-central-1b" {
  subnet_id      = aws_subnet.front-end-net-eu-central-1b.id
  route_table_id = aws_route_table.codica-rt.id
}

resource "aws_security_group" "codica-sg" {
  name        = "ssh-web"
  description = "Allow 22 and 80 ports traffic"
  vpc_id      = aws_vpc.codica-vpc.id

  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "WEB from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ssh-web-sg"
  }
}

data "aws_ami" "ubuntu-latest" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] 
}
resource "aws_instance" "ApacheUbuntu" {
  ami           = data.aws_ami.ubuntu-latest.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.front-end-net-eu-central-1a.id
  vpc_security_group_ids = [aws_security_group.codica-sg.id]
  associate_public_ip_address = true

  key_name = "terraform"
  

  tags = {
    Name = "ApacheUbuntu"
  }
}

output "ApacheUbuntu_public_ip" {
  value = aws_instance.ApacheUbuntu.public_ip
}

resource "aws_instance" "ApacheRedHat" {
  ami           = "ami-03f255060aa887525" 
  instance_type = "t2.micro"
  subnet_id = aws_subnet.front-end-net-eu-central-1b.id
  vpc_security_group_ids = [aws_security_group.codica-sg.id]
  associate_public_ip_address = true

  key_name = "terraform"
  

  tags = {
    Name = "ApacheRedHat"
  }
}

output "ApacheRedHat_public_ip" {
  value = aws_instance.ApacheRedHat.public_ip
}
resource "aws_eip" "nat_gateway" {
  vpc = true
}

 resource "aws_nat_gateway" "codica-nat-gateway" {
  allocation_id = aws_eip.nat_gateway.id
  subnet_id = aws_subnet.front-end-net-eu-central-1a.id
  tags = {
    "Name" = "global nat gateway"
  }
}


resource "aws_route_table" "codica-rt-nat" {
  vpc_id = aws_vpc.codica-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.codica-nat-gateway.id
  } 

  tags = {
    Name = "codica-rt-back"
  }
}


resource "aws_route_table_association" "back-end-net-eu-central-1a-rt" {
  subnet_id      = aws_subnet.back-end-net-eu-central-1a.id
  route_table_id = aws_route_table.codica-rt-nat.id
}

resource "aws_route_table_association" "back-end-net-eu-central-1b-rt" {
  subnet_id      = aws_subnet.back-end-net-eu-central-1b.id
  route_table_id = aws_route_table.codica-rt.id
}


resource "aws_instance" "Linux-back-1a" {
  ami           = data.aws_ami.ubuntu-latest.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.back-end-net-eu-central-1a.id
  vpc_security_group_ids = [aws_security_group.codica-sg.id]
  #associate_public_ip_address = true

  key_name = "terraform" 
  

  tags = {
    Name = "Linux-a"
  }
}

resource "aws_instance" "Linux-back-1b" {
  ami           = data.aws_ami.ubuntu-latest.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.back-end-net-eu-central-1b.id
  vpc_security_group_ids = [aws_security_group.codica-sg.id]
  #associate_public_ip_address = true

  key_name = "terraform" 
  

  tags = {
    Name = "Linux-b"
  }
}

resource "aws_lb_target_group_attachment" "codica-lb-tg" {    
  target_group_arn = "${aws_lb_target_group.codica-lb.arn}"
  target_id        = "${aws_instance.ApacheUbuntu.id}"
  port             = 80 
}

resource "aws_lb_target_group_attachment" "codica-lb-tg1" {
  target_group_arn = "${aws_lb_target_group.codica-lb.arn}"
  target_id        = "${aws_instance.ApacheRedHat.id}"
  port             = 80 
}

resource "aws_lb_target_group" "codica-lb" {
  name     = "codica-lb"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.codica-vpc.id

  load_balancing_algorithm_type = "round_robin"

  health_check {
    enabled             = true
    path                = "/inbex.html"
    port                = 80
    protocol            = "HTTP"
    healthy_threshold   = 3
    unhealthy_threshold = 3
    matcher             = "200-499"
  }
}

resource "aws_lb" "codica-lb" {
  name               = "codica-lb-tf"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.codica-sg.id}"]
  subnets            = [aws_subnet.front-end-net-eu-central-1a.id, aws_subnet.front-end-net-eu-central-1b.id]
  enable_cross_zone_load_balancing = true
  #enable_deletion_protection = true

  tags = {
    Environment = "production"
  }
}

resource "aws_lb_listener" "codica-lb" {
  load_balancer_arn = aws_lb.codica-lb.arn
  port              = "80"
  protocol          = "HTTP"   

  default_action {
    type            = "forward"
    target_group_arn = aws_lb_target_group.codica-lb.arn
  }
  
}