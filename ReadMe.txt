Description:
I used terraform to create infrastructure in AWS and Ansible-playbook to install Apache web-server from RHEL and Debian-like operating systems.
----------------------------
AWS infrastructure looks:
VPC: 
-codica-vpc 10.0.0.0/16.

Subnets:
- front-end-net-eu-central-1a - 10.0.1.0/24.
- front-end-net-eu-central-1b - 10.0.2.0/24.
front-end-nets use resource "aws_internet_gateway" "codica-gw".

- back-end-net-eu-central-1a - 10.0.3.0/24.
- back-end-net-eu-central-1b - 10.0.4.0/24.
back-end-nets use resource "aws_eip" "nat_gateway".

EC2:
EC2-instance server ApacheUbuntu in front-end subnet.
EC2-instance server ApacheUbuntu in front-end subnet.

EC2-instance server Ubuntu last vesion without soft in back-end-net.
EC2-instance server Ubuntu last vesion without soft in back-end-net.

Security group:
- codica-sg allow 22 and 80 ports traffic.

Other:
Application load balancer, target group, route tables and other required resources.
----------------------------
Terraform:
- main.tf file, use it to see code or create infrastructure in AWS.
----------------------------
Ansible:
- hosts.txt
- ansible.cfg
- play-book Apache.yml - use it to install Apache and create web-page for RHEL and Debian-like operating systems.
----------------------------
Connection data:

Load balancer: http://codica-lb-tf-515074164.eu-central-1.elb.amazonaws.com/

Front-end Apache web-servers:
3.75.249.227 ApacheUbuntu user=ubuntu.
3.126.55.54  ApacheRedHat user=ec2-user.

back-end-net Linux-servers:
10.0.3.175 Ubuntu-a user=ubuntu.
10.0.4.238 Ubuntu user=ubuntu.

There was added public key ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILWGIWLEftb2/+SNvxxSBCLn21T0GKc6i7eaSaPa7OFn
ivankarpenko@codica to all servers.

Created by Yevhen Khochenkov
P.S. I had been doing it for 7 hours.
